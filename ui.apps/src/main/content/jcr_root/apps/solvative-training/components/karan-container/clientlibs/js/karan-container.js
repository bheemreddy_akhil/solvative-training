(() => {
    'use strict';
    class Container {
        constructor(el) {
            if (el) {
                this.$el = el;
            }
        }
    }

    window.addEventListener('DOMContentLoaded', () => {
        const containerEls = document.querySelectorAll('.karan-container__component');
        if (containerEls?.length) {
            const containerElsArr = Array.from(containerEls);
            containerElsArr.forEach((containerEl) => {
                if (!containerEl.classList.contains('karan-container--mounted')) {
                    containerEl.classList.add('karan-container--mounted')
                    new Container(containerEl);
                }
            });
        }
    });
})();
