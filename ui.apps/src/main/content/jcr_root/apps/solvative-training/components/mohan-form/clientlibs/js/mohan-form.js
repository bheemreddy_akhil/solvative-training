(() => {
    'use strict';

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        const values = [...formData.entries()];
        alert(
            "You've chosen " + "\n" +
            values[0][0] + " : " + values[0][1] + " , " + "\n" +  values[1][0] + " : " + values[1][1] + " ."
        );
    }
    window.addEventListener('DOMContentLoaded', () => {
        const mohanFormsEl = document.querySelectorAll('.mohan-form__component');
        if (mohanFormsEl?.length) {
            mohanFormsEl.forEach((mohanFormEl) => {
                mohanFormEl.addEventListener("submit", handleSubmit);
            });
        }
    });
})();