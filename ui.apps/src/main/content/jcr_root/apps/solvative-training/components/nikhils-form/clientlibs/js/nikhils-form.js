(() => {
  "use strict";
  const formEl = document.querySelector(".nikhils-form--component");

  const handleSubmit = (event) => {
    let form = new FormData(event.target);
    let user = form.get("name");
    alert(`Thank you @${user}`);
  };

  window.addEventListener("DOMContentLoaded", () => {
    if (formEl) {
      formEl.addEventListener("submit", (e) =>{
         e.preventDefault(); 
         handleSubmit(e);
        });
      if (!formEl.classList.contains("nikhils-form--component--mounted")) {
        formEl.classList.add("nikhils-form--component--mounted");
      }
    }
  });
})();
