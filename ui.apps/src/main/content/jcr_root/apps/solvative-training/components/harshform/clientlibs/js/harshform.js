(() => {
  "use strict";
  const handleSubmit = (e) => {
    e.preventDefault();
    const form = new FormData(e.target);
    const user = form.get("city");
    alert(`Thank you @${user}`);
  };
  window.addEventListener("DOMContentLoaded", () => {
    const formEl = document.querySelector(".harshform__component");
    if (formEl) {
      formEl.addEventListener("submit", handleSubmit);
      if (!formEl.classList.contains("harshform__component--mounted")) {
        formEl.classList.add("harshform__component--mounted");
      }
    }
  });
})();
