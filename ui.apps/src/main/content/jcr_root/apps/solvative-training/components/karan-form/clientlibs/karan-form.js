(() => {
    "use strict";

    function Profile(name, city) {
        this.name = name;
        this.city = city;

        this.displayInfo = () => {
            console.log("displayInfo called: ", this.name, this.city);
            alert("Hi " + this.name + ",\nYour city is: " + this.city);
        };
    }

    const handleSubmit = (e) => {
        try {
            e.preventDefault();
            const form = new FormData(e.target);
            const user = new Profile(form.get("name"), form.get("city"));
            user.displayInfo();
        } catch (e) {
            console.log("Error in form submission ->", e);
            alert(e.message);
        }
    };

    window.addEventListener("DOMContentLoaded", () => {
        const formEl = document.querySelector(".karan-form__component");
        if (formEl) {
            formEl.addEventListener("submit", handleSubmit);
            if (!formEl.classList.contains("karan-form__component--mounted")) {
                formEl.classList.add("karan-form__component--mounted");
            }
        } else {
            console.log("karan-form__component element not found");
            alert("Form Element not found");
        }
    });
})();
