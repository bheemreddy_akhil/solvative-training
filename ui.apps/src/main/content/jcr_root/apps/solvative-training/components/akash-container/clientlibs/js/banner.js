(() => {
    'use strict';
    class Banner {
        constructor(el) {
            if (el) {
                this.$el = el;
            }
        }
    }

    window.addEventListener('DOMContentLoaded', () => {
        const bannerEls = document.querySelectorAll('.banner__component');
        if (bannerEls?.length) {
            const bannerElsArr = Array.from(bannerEls);
            bannerElsArr.forEach((bannerEl) => {
                if (!bannerEl.classList.contains('banner--mounted')) {
                    bannerEl.classList.add('banner--mounted')
                    new Banner(bannerEl);
                }
            });
        }
    });
})();