(() => {
  "use strict";

  window.addEventListener("DOMContentLoaded", () => {
    const cardEle = document.querySelectorAll(".nikhils-card__container");
    if (cardEle?.length) {
      const cardArr = Array.from(cardEle);
      cardArr.forEach((cardEle) => {
        if (!cardEle.classList.contains("nikhils-card__container--mounted")) {
          cardEle.classList.add("nikhils-card__container--mounted");
          const btnArr = document.querySelectorAll(
            ".nikhils-card__container--btn"
          );
          console.log(btnArr);
        }
      });
    }
  });
})();
